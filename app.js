let mongoClient = require("mongodb").MongoClient;
let express=require("express");
let app=express();
let url = "mongodb://localhost:27017/SmallTask";
app.use(express.static(__dirname+"static"));

conn=mongoClient.connect(url);
app.get("/pricing",function (request,response) {
    //console.log();
    let coupon=request._parsedOriginalUrl.query;
    if(coupon){
        console.log(coupon);
        conn.then(function (db) {
        db.collection("PricingData").findOne({couponName: coupon},{_id:0},function(err, results){
            console.log(results);
            response.send(JSON.stringify(results));
            response.end();
        });
    })
        .catch(function (err) {
            console.log(err);
        })
    }
    else {
        conn.then(function (db) {
            db.collection("PricingData").findOne({couponName: ""},{_id:0},function(err, results){
                console.log(results);
                response.send(JSON.stringify(results));
                response.end();
            });
        })
            .catch(function (err) {
                console.log(err);
            })
    }

});

process
    .on('SIGTERM', function() {
        console.log("\nTerminating");
        process.exit(0);
    })
    .on('SIGINT', function() {
        console.log("\nTerminating");
        process.exit(0);
    });

app.listen(3000,function () {
    console.log("Server running");
});

module.exports.app = app;