const request = require("supertest");

let app=require("./app").app;

it("Should return json",function (done) {
   request(app)
       .get("/pricing")
       .expect("{\"couponName\":\"\",\"pricingData\":{\"description\":\"Some new data\"},\"createdAt\":\"2010-03-18T16:00:00.000Z\"}")
       .end(done);
});